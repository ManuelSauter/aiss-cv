# AISS-CV Pandemic Package :book:
[![pipeline status](https://git.scc.kit.edu/ukojp/aiss-cv/badges/master/pipeline.svg?key_text=Code+Quality&key_width=90)](https://git.scc.kit.edu/ukojp/aiss-cv/-/commits/master)
[![Build status](https://dev.azure.com/JoelOswald/AISSCV/_apis/build/status/AISSCV-Python%20package-CI?branchName=master)](https://dev.azure.com/JoelOswald/AISSCV/_build/latest?definitionId=1)
[![coverage report](https://git.scc.kit.edu/ukojp/aiss-cv/badges/master/coverage.svg?job=coverage)](https://git.scc.kit.edu/ukojp/aiss-cv/-/commits/master)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)


Welcome to the repository of our group "Pandemic Package", an AISS-CV project for the Summer Term 2021. This repository includes an object detection application for damages of packages based on state-of-the-art object detection frameworks. It is geared towards deployment on a Jetson Nano for on-edge inferences.

## Our Documentation
The ReadMe files do not belong to our Documentation. We used Sphinx for a detailed documentation of our project, the system and the code itself. 
Please follow these steps to view the documentation as html files on your local machine :
1.  Clone this Repo
2.  Open the [index.html](docs/build/html/index.html) in the [docs/build/html](docs/build/html) with a browser of your choice. 

Alternatively, you can download a PDF version of the documentation [here](docs/build/latex/AISS-CV.pdf).

## Final Presentation
Our [Final Presentation as PDF](docs/Final Presentation - PandemicPackage.pdf) is stored in the docs folder.
Same goes for the [pptx version](docs/AISSCV-PandemicPackage-Final Presentation.pptx). 

## Data
All images and annotations are stored in [BwCloud](https://bwsyncandshare.kit.edu/s/s5Yr4QQrCEfdigo).

In addition all necessary data, as well as trained models are stored in our Azure File Share. Please contact [Joel](mailto:joel.oswald@student.kit.edu) to get access.

# Tutorials for Users & Developers:
Moved [here](Developer_Tutorials.md)

# 
<center><img src="docs/source/img/final_example2.png" alt="Example Detection" width="800"/></center>
