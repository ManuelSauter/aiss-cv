\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Summary}{3}{chapter.1}%
\contentsline {chapter}{\numberline {2}Project Description}{5}{chapter.2}%
\contentsline {section}{\numberline {2.1}Use Case}{5}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}Problem Setting}{5}{subsection.2.1.1}%
\contentsline {subsection}{\numberline {2.1.2}Real\sphinxhyphen {}Life Value}{6}{subsection.2.1.2}%
\contentsline {subsection}{\numberline {2.1.3}General Approach}{7}{subsection.2.1.3}%
\contentsline {subsubsection}{Step 1: Identifying Packages}{7}{subsubsection*.3}%
\contentsline {subsubsection}{Step 2: Localizing and Categorizing Critical Areas}{8}{subsubsection*.4}%
\contentsline {subsubsection}{Step 3: Interpretation and Warning Message}{9}{subsubsection*.5}%
\contentsline {section}{\numberline {2.2}Collaboration}{10}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Our Team}{11}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}Co\sphinxhyphen {}Working Environment}{11}{subsection.2.2.2}%
\contentsline {section}{\numberline {2.3}Timeline}{12}{section.2.3}%
\contentsline {subsection}{\numberline {2.3.1}Brief Timeline}{12}{subsection.2.3.1}%
\contentsline {subsection}{\numberline {2.3.2}Detailed Timeline}{12}{subsection.2.3.2}%
\contentsline {subsubsection}{April}{12}{subsubsection*.6}%
\contentsline {subsubsection}{May}{12}{subsubsection*.7}%
\contentsline {subsubsection}{June}{14}{subsubsection*.8}%
\contentsline {subsubsection}{July}{14}{subsubsection*.9}%
\contentsline {section}{\numberline {2.4}Training Data Collection}{14}{section.2.4}%
\contentsline {subsection}{\numberline {2.4.1}Overall Information on the First Round of Training Data Collection}{15}{subsection.2.4.1}%
\contentsline {subsection}{\numberline {2.4.2}Classes of Damages}{15}{subsection.2.4.2}%
\contentsline {subsection}{\numberline {2.4.3}Standards and Tools for Training Data Collection}{17}{subsection.2.4.3}%
\contentsline {subsection}{\numberline {2.4.4}Image Annotation}{17}{subsection.2.4.4}%
\contentsline {subsection}{\numberline {2.4.5}End of First Round of Training Data Collection}{17}{subsection.2.4.5}%
\contentsline {subsection}{\numberline {2.4.6}Additional Phase of Training Data Collection in July}{18}{subsection.2.4.6}%
\contentsline {section}{\numberline {2.5}Data Augmentation}{18}{section.2.5}%
\contentsline {subsection}{\numberline {2.5.1}Scientific Inspiration}{18}{subsection.2.5.1}%
\contentsline {subsection}{\numberline {2.5.2}Augmentation Script}{18}{subsection.2.5.2}%
\contentsline {subsection}{\numberline {2.5.3}Result}{19}{subsection.2.5.3}%
\contentsline {subsection}{\numberline {2.5.4}Side Notes}{20}{subsection.2.5.4}%
\contentsline {section}{\numberline {2.6}Model Comparison and Choice for the Project}{20}{section.2.6}%
\contentsline {subsection}{\numberline {2.6.1}Two\sphinxhyphen {}Stage Detectors}{20}{subsection.2.6.1}%
\contentsline {subsection}{\numberline {2.6.2}One\sphinxhyphen {}Stage Detectors}{20}{subsection.2.6.2}%
\contentsline {subsection}{\numberline {2.6.3}Comparison and Project Choice}{21}{subsection.2.6.3}%
\contentsline {section}{\numberline {2.7}Outlook \& Possible Enhancements}{21}{section.2.7}%
\contentsline {subsection}{\numberline {2.7.1}Real\sphinxhyphen {}Life Insights and Fine\sphinxhyphen {}Tuning for Productive Environment}{21}{subsection.2.7.1}%
\contentsline {subsubsection}{Relevant Critical Properties}{21}{subsubsection*.10}%
\contentsline {subsubsection}{Interpretation of Critical Properties}{21}{subsubsection*.11}%
\contentsline {subsection}{\numberline {2.7.2}Two\sphinxhyphen {}Stage Model Architecture}{22}{subsection.2.7.2}%
\contentsline {subsection}{\numberline {2.7.3}Implementation of Polygon Labels \& Instance Segmentation}{22}{subsection.2.7.3}%
\contentsline {subsection}{\numberline {2.7.4}Deployment: Integration into a Warehouse Management System (WMS)}{22}{subsection.2.7.4}%
\contentsline {section}{\numberline {2.8}Learnings}{23}{section.2.8}%
\contentsline {subsection}{\numberline {2.8.1}Training Data Collection}{23}{subsection.2.8.1}%
\contentsline {subsection}{\numberline {2.8.2}Choice of Models and Training}{24}{subsection.2.8.2}%
\contentsline {subsection}{\numberline {2.8.3}Co\sphinxhyphen {}Working \& Communication}{24}{subsection.2.8.3}%
\contentsline {chapter}{\numberline {3}System Description}{25}{chapter.3}%
\contentsline {section}{\numberline {3.1}Architecture Overview}{25}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}CI/CD Pipelines}{25}{subsection.3.1.1}%
\contentsline {subsubsection}{GitLab Pipelines}{26}{subsubsection*.12}%
\contentsline {paragraph}{Code Quality}{26}{paragraph*.13}%
\contentsline {paragraph}{Coverage}{26}{paragraph*.14}%
\contentsline {subsubsection}{Azure Pipeline}{26}{subsubsection*.15}%
\contentsline {subsection}{\numberline {3.1.2}Jetson}{27}{subsection.3.1.2}%
\contentsline {subsubsection}{The Hardware}{27}{subsubsection*.17}%
\contentsline {subsubsection}{Access for the Team}{28}{subsubsection*.18}%
\contentsline {subsubsection}{Software Deployment}{28}{subsubsection*.19}%
\contentsline {subsubsection}{Model Deployment}{28}{subsubsection*.20}%
\contentsline {subsection}{\numberline {3.1.3}Azure}{29}{subsection.3.1.3}%
\contentsline {subsubsection}{Container Registry}{29}{subsubsection*.21}%
\contentsline {subsubsection}{File Share}{30}{subsubsection*.22}%
\contentsline {subsubsection}{Container Instances}{30}{subsubsection*.23}%
\contentsline {subsubsection}{Kubernetes}{30}{subsubsection*.24}%
\contentsline {section}{\numberline {3.2}Object Detection}{30}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}YOLO and Darknet}{31}{subsection.3.2.1}%
\contentsline {subsubsection}{Installation}{31}{subsubsection*.25}%
\contentsline {subsubsection}{Training Configuration}{31}{subsubsection*.26}%
\contentsline {subsubsection}{Updating Labels}{32}{subsubsection*.27}%
\contentsline {subsubsection}{Local Training}{32}{subsubsection*.28}%
\contentsline {subsubsection}{Training with Docker}{33}{subsubsection*.29}%
\contentsline {subsubsection}{Training in Azure}{33}{subsubsection*.30}%
\contentsline {subsubsection}{Used Models and Performance}{33}{subsubsection*.31}%
\contentsline {paragraph}{YOLOv4}{33}{paragraph*.32}%
\contentsline {paragraph}{YOLOv4\sphinxhyphen {}Tiny}{34}{paragraph*.33}%
\contentsline {subsection}{\numberline {3.2.2}Tensorflow Object Detection API}{34}{subsection.3.2.2}%
\contentsline {subsubsection}{Preperation}{34}{subsubsection*.34}%
\contentsline {subsubsection}{Training}{35}{subsubsection*.35}%
\contentsline {subsubsection}{Evaluation}{36}{subsubsection*.36}%
\contentsline {section}{\numberline {3.3}Code Overview}{37}{section.3.3}%
\contentsline {section}{\numberline {3.4}Subsequent Damage Handling}{38}{section.3.4}%
\contentsline {subsection}{\numberline {3.4.1}Description of Business Logic in the Software}{38}{subsection.3.4.1}%
\contentsline {subsection}{\numberline {3.4.2}Examples of Results}{38}{subsection.3.4.2}%
\contentsline {chapter}{\numberline {4}Code Documentation}{41}{chapter.4}%
\contentsline {section}{\numberline {4.1}Inference}{41}{section.4.1}%
\contentsline {subsection}{\numberline {4.1.1}Camera module}{41}{subsection.4.1.1}%
\contentsline {subsection}{\numberline {4.1.2}Control Loop module}{42}{subsection.4.1.2}%
\contentsline {subsection}{\numberline {4.1.3}Display module}{43}{subsection.4.1.3}%
\contentsline {subsection}{\numberline {4.1.4}Model Interface}{43}{subsection.4.1.4}%
\contentsline {subsection}{\numberline {4.1.5}SSD\sphinxhyphen {}TF module}{43}{subsection.4.1.5}%
\contentsline {subsection}{\numberline {4.1.6}Visualization module}{44}{subsection.4.1.6}%
\contentsline {subsection}{\numberline {4.1.7}Yolo module}{44}{subsection.4.1.7}%
\contentsline {section}{\numberline {4.2}Augmentation}{45}{section.4.2}%
\contentsline {subsection}{\numberline {4.2.1}Augment Dataset module}{45}{subsection.4.2.1}%
\contentsline {subsection}{\numberline {4.2.2}Augment Dataset with damages module}{46}{subsection.4.2.2}%
\contentsline {section}{\numberline {4.3}Utils}{47}{section.4.3}%
\contentsline {subsection}{\numberline {4.3.1}Crop\sphinxhyphen {}Resize module}{47}{subsection.4.3.1}%
\contentsline {subsection}{\numberline {4.3.2}Label Helper module}{47}{subsection.4.3.2}%
\contentsline {subsection}{\numberline {4.3.3}Label Reader module}{49}{subsection.4.3.3}%
\contentsline {subsection}{\numberline {4.3.4}Negative Examples module}{50}{subsection.4.3.4}%
\contentsline {subsection}{\numberline {4.3.5}Pascal\sphinxhyphen {}VOC to tf\sphinxhyphen {}record module}{50}{subsection.4.3.5}%
\contentsline {subsection}{\numberline {4.3.6}Pascal\sphinxhyphen {}VOC to yolo module}{51}{subsection.4.3.6}%
\contentsline {subsection}{\numberline {4.3.7}Proto to Tensorboard module}{52}{subsection.4.3.7}%
\contentsline {section}{\numberline {4.4}Unit Tests}{52}{section.4.4}%
\contentsline {subsection}{\numberline {4.4.1}Test module}{52}{subsection.4.4.1}%
\contentsline {chapter}{Python Module Index}{53}{section*.116}%
\contentsline {chapter}{Index}{55}{section*.117}%
