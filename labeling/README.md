# Our Labeling
To label your images, please open [the html file](packaging.html) (simply double-click from your file-explorer) with a browser of your choice. 
An tutorial with several steps to take will be shown. 
Don't hestiate to ask, if something is unclear. 
P.S. You will also need the packaging_project.json which includes our labels. 
P.P.S. please remember: Box is a box, everything else should be a polygon!
## Example Images: 
![Box only](screenshot_box2.png)
![Box with damages](screenshot_damaged.png)